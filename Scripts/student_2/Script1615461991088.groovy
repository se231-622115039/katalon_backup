import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By

WebUI.openBrowser('')

WebUI.navigateToUrl('http://54.90.54.5:8121/')

WebUI.setText(findTestObject('Object Repository/Page_Lab 234/input_Password_studentId'), '58')

WebUI.click(findTestObject('Page_Lab 234/button_Check students'))

WebUI.verifyElementText(findTestObject('Page_Lab 234/td_result_id_1'), '5821105002')

WebUI.verifyElementText(findTestObject('Page_Lab 234/td_result_name_1'), 'Somluck')

WebUI.verifyElementText(findTestObject('Page_Lab 234/td_result_surname_1'), 'Kamsing')

WebUI.verifyElementText(findTestObject('Page_Lab 234/td_result_gpa_1'), '2.50')

WebUI.verifyElementText(findTestObject('Page_Lab 234/td_result_id_2'), '5821105003')

WebUI.verifyElementText(findTestObject('Page_Lab 234/td_result_name_2'), 'Cherprang')

WebUI.verifyElementText(findTestObject('Page_Lab 234/td_result_surname_2'), 'Capt,cher')

WebUI.verifyElementText(findTestObject('Page_Lab 234/td_result_gpa_2'), '3.50')

WebUI.verifyElementText(findTestObject('Page_Lab 234/p_Total GPA'), 'Total GPA: 3.00')


WebDriver driver = DriverFactory.getWebDriver()

WebElement Table = driver.findElement(By.xpath("//*[@id='resultTable']"))

List<WebElement> Rows = Table.findElements(By.tagName('tr'))

println('No. of rows: ' + Rows.size())

WebUI.verifyEqual(2,Rows.size())


WebUI.closeBrowser()

