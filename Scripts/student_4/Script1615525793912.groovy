import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

List<List<String>> data = findTestData('Data Files/student_3 test dat').getAllData()

WebUI.openBrowser('')

WebUI.navigateToUrl('http://54.90.54.5:8121/')

for(List<String> i : data) {
	print(i.get(0) + " " + i.get(1)+ " " + i.get(2)+ " " + i.get(3)+ " " + i.get(4))
	WebUI.setText(findTestObject('Object Repository/FinalTest/Page_Lab 234/input_Password_studentId'), i.get(0))
	
	WebUI.click(findTestObject('Object Repository/FinalTest/Page_Lab 234/button_Check student'))
	   
	WebUI.verifyElementText(findTestObject('FinalTest/Page_Lab 234/td_result_sid'), i.get(0))
	
	WebUI.verifyElementText(findTestObject('FinalTest/Page_Lab 234/td_result_name'), i.get(1))
	
	WebUI.verifyElementText(findTestObject('FinalTest/Page_Lab 234/td_result_surname'), i.get(2))
	
	WebUI.verifyElementText(findTestObject('FinalTest/Page_Lab 234/td_result_gpa'), i.get(3))
	
	WebUI.verifyElementText(findTestObject('FinalTest/Page_Lab 234/p_Total GPA'), i.get(4))
	
	WebUI.setText(findTestObject('Object Repository/FinalTest/Page_Lab 234/input_Password_studentId'), "")
}


WebUI.closeBrowser()